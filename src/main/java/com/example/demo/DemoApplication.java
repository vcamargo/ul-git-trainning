package com.example.demo;

import org.springframework.boot.*;
import org.springframework.boot.autoconfigure.*;
import org.springframework.web.bind.annotation.*;

@SpringBootApplication
@RestController
public class DemoApplication {

	@GetMapping("/")
	String home() {
		return "Spring is here!";
	}

	@GetMapping("/home")
	String newHome() {
		return "Spring is here at /home!";
	}

	@GetMapping("/home1")
	String newHome1() {
		return "Spring is here at /home1!";
	}

    @GetMapping("/help")
    String help() {
        return "Spring is here at /help!";
    }

	public static void main(String[] args) {
		SpringApplication.run(DemoApplication.class, args);
	}
}